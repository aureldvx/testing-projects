CWD := $(shell docker compose run php pwd)
TOOLS_DIR := $(CWD)/tools
COMPOSE_RUN := docker compose run --rm
COMPOSE_PHP := $(COMPOSE_RUN) php
COMPOSE_COMPOSER := $(COMPOSE_RUN) composer
COMPOSE_SYMFONY := $(COMPOSE_RUN) symfony

fresh_install:
	$(COMPOSE_PHP) chmod +x /var/www/html/docker/php/install_tools.sh
	$(COMPOSE_PHP) chmod +x /var/www/html/docker/php/fresh_install.sh
	$(COMPOSE_PHP) $(CWD)/docker/php/fresh_install.sh

install_tools:
	$(COMPOSE_PHP) chmod +x /var/www/html/docker/php/install_tools.sh
	$(COMPOSE_PHP) $(CWD)/docker/php/install_tools.sh

install:
	$(COMPOSE_COMPOSER) install
	$(COMPOSE_COMPOSER) install --working-dir=$(TOOLS_DIR)/phpmd
	$(COMPOSE_COMPOSER) install --working-dir=$(TOOLS_DIR)/phpcs
	$(COMPOSE_COMPOSER) install --working-dir=$(TOOLS_DIR)/phpcsfixer
	$(COMPOSE_COMPOSER) install --working-dir=$(TOOLS_DIR)/phpstan
	$(COMPOSE_COMPOSER) install --working-dir=$(TOOLS_DIR)/phpcpd

fix:
	$(COMPOSE_PHP) $(TOOLS_DIR)/php-cs-fixer/vendor/bin/php-cs-fixer fix

php_stan:
	$(COMPOSE_PHP) $(TOOLS_DIR)/phpstan/vendor/bin/phpstan analyse -c phpstan.neon src

phpmd:
	$(COMPOSE_PHP) $(TOOLS_DIR)/phpmd/vendor/bin/phpmd src/ text .phpmd.xml

phpcs:
	$(COMPOSE_PHP) $(TOOLS_DIR)/phpcs/vendor/bin/phpcs -s --standard=phpcs.xml.dist

php_cbf:
	$(COMPOSE_PHP) $(TOOLS_DIR)/phpcs/vendor/bin/phpcbf --standard=phpcs.xml.dist ./src ./tests

php_cpd:
	$(COMPOSE_PHP) $(TOOLS_DIR)/phpcpd/vendor/bin/phpcpd src/

lint_back:
	make phpmd
	make phpcpd
	make phpcs
	make php_stan
	make fix
	make php_cbf
