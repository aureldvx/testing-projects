#!/bin/bash

CURRENT_DIR=$PWD
TOOLS_DIR="$CURRENT_DIR/tools"

install_php_mess_detector() {
  cd "$TOOLS_DIR" || exit
  mkdir phpmd && cd phpmd || exit
  echo -e "n" | composer require --dev phpmd/phpmd
}

install_php_code_sniffer() {
  cd "$TOOLS_DIR" || exit
  mkdir phpcs && cd phpcs || exit
  echo -e "n" | composer require --dev squizlabs/php_codesniffer escapestudios/symfony2-coding-standard
}

install_php_cs_fixer() {
  cd "$TOOLS_DIR" || exit
  mkdir phpcsfixer && cd phpcsfixer || exit
  echo -e "n" | composer require --dev friendsofphp/php-cs-fixer
}

install_php_stan() {
  cd "$TOOLS_DIR" || exit
  mkdir phpstan && cd phpstan || exit
  echo -e "n" | composer require --dev phpstan/phpstan phpstan/phpstan-symfony phpstan/phpstan-doctrine
}

install_php_copy_paste_detector() {
  cd "$TOOLS_DIR" || exit
  mkdir phpcpd && cd phpcpd || exit
  echo -e "n" | composer require --dev sebastian/phpcpd
}

update_gitignore() {
  echo "
###> php-tooling ###
.DS_Store
.php-cs-fixer.cache
.phpcs.cache
.vscode/
vendor/
node_modules/
###< php-tooling ###" >>.gitignore
}

manage_config_files() {
  cd "$CURRENT_DIR" || exit
  update_gitignore
}

launch_install() {
  mkdir -p "$TOOLS_DIR" || exit
  install_php_mess_detector
  install_php_code_sniffer
  install_php_cs_fixer
  install_php_stan
  install_php_copy_paste_detector
  manage_config_files
}

launch_install
