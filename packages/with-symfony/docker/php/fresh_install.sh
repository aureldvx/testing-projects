#!/bin/bash

init() {
    symfony new app
    mv app/{*,.*} ./
    rm -rf app
    docker/php/install_tools.sh
    composer install
}

init
